<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Task manager</title>
</head>
<style>
    h1 {
        font-size: 1.6em;
    }

    a {
        color: darkblue;
    }
</style>
<body>

<table width="100%" height="100%" border="1">
    <tr>
        <td height="35" width="200" nowrap="nowrap" align="center">Task manager</td>
        <td align="right">
            <a href="/projects">Projects</a>
            <a href="/tasks">Tasks</a>
        </td>
    </tr>
    <tr>
        <td colspan="2" valign="top">
