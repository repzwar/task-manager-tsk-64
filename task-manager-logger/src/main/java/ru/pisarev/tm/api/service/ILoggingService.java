package ru.pisarev.tm.api.service;

import ru.pisarev.tm.dto.LoggerDTO;

public interface ILoggingService {

    void writeLog(LoggerDTO message);

}
