CREATE TABLE public.tm_user
(
    id character varying(250) COLLATE pg_catalog."default" NOT NULL,
    login character varying(250) COLLATE pg_catalog."default" NOT NULL,
    password_hash character varying(250) COLLATE pg_catalog."default",
    email character varying(250) COLLATE pg_catalog."default",
    first_name character varying(250) COLLATE pg_catalog."default",
    last_name character varying(250) COLLATE pg_catalog."default",
    middle_name character varying(250) COLLATE pg_catalog."default",
    role character varying(250) COLLATE pg_catalog."default",
    locked boolean,
    CONSTRAINT user_pkey PRIMARY KEY (id)
);

CREATE TABLE public.tm_session
(
    id character varying(255) COLLATE pg_catalog."default" NOT NULL,
    "timestamp" bigint NOT NULL,
    signature character varying(255) COLLATE pg_catalog."default" NOT NULL,
    user_id character varying(255) COLLATE pg_catalog."default" NOT NULL,
    CONSTRAINT session_pkey PRIMARY KEY (id)
);
CREATE TABLE public.tm_project
(
    id character varying(255) COLLATE pg_catalog."default" NOT NULL,
    name character varying(255) COLLATE pg_catalog."default" NOT NULL,
    description character varying(255) COLLATE pg_catalog."default",
    status character varying COLLATE pg_catalog."default" NOT NULL,
    start_date date,
    finish_date date,
    user_id character varying COLLATE pg_catalog."default",
    created date,
    CONSTRAINT project_pkey PRIMARY KEY (id),
    CONSTRAINT "user" FOREIGN KEY (user_id)
        REFERENCES public.tm_user (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION
        NOT VALID
);
CREATE TABLE public.tm_task
(
    id character varying(255) COLLATE pg_catalog."default" NOT NULL,
    name character varying(255) COLLATE pg_catalog."default" NOT NULL,
    description character varying(255) COLLATE pg_catalog."default",
    status character varying COLLATE pg_catalog."default" NOT NULL,
    project_id character varying(255) COLLATE pg_catalog."default",
    start_date date,
    finish_date date,
    user_id character varying(255) COLLATE pg_catalog."default",
    created date,
    CONSTRAINT task_pkey PRIMARY KEY (id),
    CONSTRAINT task_project_fkey FOREIGN KEY (project_id)
        REFERENCES public.tm_project (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION,
    CONSTRAINT "user" FOREIGN KEY (user_id)
        REFERENCES public.tm_user (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION
        NOT VALID
);
