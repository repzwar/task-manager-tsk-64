package ru.pisarev.tm.repository.model;

import org.jetbrains.annotations.Nullable;
import org.springframework.data.jpa.repository.JpaRepository;
import ru.pisarev.tm.model.SessionGraph;

import java.util.List;

public interface ISessionRepository extends JpaRepository<SessionGraph, String> {

    List<SessionGraph> findAllByUserId(@Nullable String userId);

    void deleteByUserId(@Nullable String userId);

}
